﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace S00126962_SSD_CA1
{
    class Profile
    {
        public string ProfileName { get; set; } //Name of the Player EG:Greg
        public string ProfileEmail { get; set; }
        public string ProfilePhone { get; set; }
        public string ProfileClass { get; set; } //Class the Player is Playing EG:Warrior
        public string ProfileClassSpec { get; set; } //Specilastion of players class EG: Fury
        public string ProfilePawnString { get; set; } //string that players get from SIMC after calcing stat values(Possible Admin Issue,players can )
        public double PrimaryWeight { get; set; } //Strength/Intellect/Agiity NOTE: calcuations are the same regardless of which one of 3 the player is using
        public double CritWeight { get; set; } //Weight of Crital Strike Rating
        public double MastWeight { get; set; } //Weight of Mastery Rating
        public double VersWeight { get; set; } //Weight of Versatilty
        public double HasteWeight { get; set; } //Weight of Haste
        public double LeechWeight { get; set; } //Weight of Leech NOTE:Only applicple to tanks and healers,weight will be 0 for others,thus not affect calcuations
        public double ArmorWeight { get; set; } //Weight of Armor stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double StamWeight { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double AvoidanceWeight { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations

        public Profile()
        {

        }

        public Profile(string v1, string v2, string v3, string v4, string v5, string v6, double v7, double v8, double v9, double v10, double v11, double v12, double v13, double v14, double v15)
        {
        }

        public Profile ReadPawnString(string pawnstring)
        {
            // Sample Pawn String :"( Pawn: v1: \"Ketsuki\": Strength =12.55, CritRating=10.35, HasteRating=10.96, MasteryRating=10.26, Versatility=10.92 )";

            Profile returnRaidMemeber = new Profile(); //return the data that we pull from the pawn string
            char[] seperatorCharacters = { ':', ',' }; //these are characters that seperate out different elements of the string
            pawnstring = pawnstring.Replace("(", ""); //get rid of the brackets that the string comes with by default
            pawnstring = pawnstring.Replace(")", "");
            pawnstring = pawnstring.Replace("Pawn: v1:", ""); //get rid of the pawn version number,again no need at all for this
            pawnstring = pawnstring.Replace("\"", ""); //might be able to get rid of this later
            string[] pawnArray = pawnstring.Split(seperatorCharacters);

            //now that the data is tidyed up and split up,we can begin to build our object

            returnRaidMemeber.ProfileName = pawnArray[0].Split('=').Last();
            returnRaidMemeber.ProfileClass = pawnArray[1].Split('=').Last();
            returnRaidMemeber.ProfileClassSpec = pawnArray[2].Split('=').Last();
            returnRaidMemeber.PrimaryWeight = Convert.ToDouble(pawnArray[3].Split('=').Last());
            //now here were things get complicated,all strings will follow the above format,but beyond this point they can be in differnt orderings
            //in the interest of simplicty on the users end,we have to abadon simplicty on our end
            //in order to ensure that the right data is assigned to the right varible in the object,we must search though the array to find the corisponding value

            foreach (var item in pawnArray)
            {
                //all characters have the below 
                if (item.Contains("CritRating"))
                {
                    returnRaidMemeber.CritWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("HasteRating"))
                {
                    returnRaidMemeber.HasteWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("MasteryRating"))
                {
                    returnRaidMemeber.MastWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Versatility"))
                {
                    returnRaidMemeber.VersWeight = Convert.ToDouble(item.Split('=').Last());
                }

                //these 4 will not be included for all specs,only certain specs will worry about the below stats
                if (item.Contains("Leech"))
                {
                    returnRaidMemeber.LeechWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Armor"))
                {
                    returnRaidMemeber.ArmorWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Avoidance")) //this might need to be changed since I cant find a character to get a pawn string with avoidance in it
                {
                    returnRaidMemeber.AvoidanceWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Stamina")) //this might need to be changed since I cant find a character to get a pawn string with avoidance in it
                {
                    returnRaidMemeber.StamWeight = Convert.ToDouble(item.Split('=').Last());
                }
            }

            return returnRaidMemeber;

        }

        public void AddProfile(Profile _newProfile)
        {
            var doc = XDocument.Load("ProfileList.xml");

            // The Restaurant class could know how to serialize itself to an XElement
            XElement newProfile = new XElement("Profile",
                  new XElement("ProfileName", _newProfile.ProfileName),
                  new XElement("ProfileEmail", _newProfile.ProfileEmail),
                  new XElement("ProfilePawnString", _newProfile.ProfilePawnString),
                  new XElement("ProfilePhone", _newProfile.ProfilePhone),
                   new XElement("ProfileClass", _newProfile.ProfileClass),
                    new XElement("ProfileSpec", _newProfile.ProfileClassSpec),
                  new XElement("PrimaryWeight", _newProfile.PrimaryWeight),
                  new XElement("CritWeight", _newProfile.CritWeight),
                  new XElement("MastWeight", _newProfile.MastWeight),
                   new XElement("VersWeight", _newProfile.VersWeight),
                   new XElement("HasteWeight", _newProfile.HasteWeight),
                   new XElement("LeechWeight", _newProfile.LeechWeight),
                   new XElement("ArmorWeight", _newProfile.ArmorWeight),
                    new XElement("StamWeight", _newProfile.StamWeight),
                   new XElement("AvoidanceWeight", _newProfile.AvoidanceWeight)
                   );

            doc.Root.Add(newProfile);
            doc.Save("ProfileList.xml");
        }


        public List<Profile> GetProfiles()
        {
            List<Profile> allProfiles = new List<Profile>();

            XmlDocument profilesXML = new XmlDocument();
            profilesXML.Load("ProfileList.xml");
            //loop though and find the class selected
            foreach (XmlNode node in profilesXML.DocumentElement.ChildNodes)
            {
                // string da = node.SelectSingleNode("ProfilePhoneNumber").InnerText;
                //values are always coming in as nulls
                Profile profileToAdd = new Profile();
                profileToAdd.ProfileName = node.SelectSingleNode("ProfileName").InnerText;
                profileToAdd.ProfileClass = node.SelectSingleNode("ProfileClass").InnerText;
                profileToAdd.ProfileClassSpec = node.SelectSingleNode("ProfileSpec").InnerText;
                profileToAdd.ProfilePawnString = node.SelectSingleNode("ProfilePawnString").InnerText;
                profileToAdd.ProfileEmail = node.SelectSingleNode("ProfileEmail").InnerText;
                profileToAdd.ProfilePhone = node.SelectSingleNode("ProfilePhone").InnerText;
                profileToAdd.PrimaryWeight = XmlConvert.ToDouble(node.SelectSingleNode("PrimaryWeight").InnerText);
                profileToAdd.CritWeight = XmlConvert.ToDouble(node.SelectSingleNode("CritWeight").InnerText);
                profileToAdd.VersWeight = XmlConvert.ToDouble(node.SelectSingleNode("VersWeight").InnerText);
                profileToAdd.HasteWeight = XmlConvert.ToDouble(node.SelectSingleNode("HasteWeight").InnerText);
                profileToAdd.MastWeight = XmlConvert.ToDouble(node.SelectSingleNode("MastWeight").InnerText);
                profileToAdd.StamWeight = XmlConvert.ToDouble(node.SelectSingleNode("StamWeight").InnerText);
                profileToAdd.LeechWeight = XmlConvert.ToDouble(node.SelectSingleNode("LeechWeight").InnerText);
                profileToAdd.ArmorWeight = XmlConvert.ToDouble(node.SelectSingleNode("ArmorWeight").InnerText);
                profileToAdd.AvoidanceWeight = XmlConvert.ToDouble(node.SelectSingleNode("AvoidanceWeight").InnerText);

                allProfiles.Add(profileToAdd);
            }

            return allProfiles;
        }

        public void deleteProfile(string _profileName)
        {
            XmlDocument profilesXML = new XmlDocument();
            profilesXML.Load("ProfileList.xml");
            var a = profilesXML.SelectSingleNode("ProfileList").ChildNodes;

            foreach (XmlNode item in profilesXML.SelectSingleNode("ProfileList").ChildNodes)
            {
                XmlNode name = item.SelectSingleNode("ProfileName");
                if (name.InnerText == _profileName)
                {
                    profilesXML.SelectSingleNode("ProfileList").RemoveChild(item);
                    profilesXML.Save("ProfileList.xml");
                }
            }
        }
    
        public void UpdateXML(Profile updatedProfile)
        {
            XmlDocument profilesXML = new XmlDocument();
            profilesXML.Load("ProfileList.xml");

            //since a user can update anything,just reassign the values again
            foreach (XmlNode item in profilesXML.SelectSingleNode("ProfileList").ChildNodes)
            {
                XmlNode name = item.SelectSingleNode("ProfileName");
                if (name.InnerText == updatedProfile.ProfileName)
                {
                    item.SelectSingleNode("ProfileName").InnerText = updatedProfile.ProfileName.ToString();
                    item.SelectSingleNode("ProfileClass").InnerText = updatedProfile.ProfileClass;
                    item.SelectSingleNode("ProfileSpec").InnerText = updatedProfile.ProfileClassSpec;
                    item.SelectSingleNode("ProfilePawnString").InnerText = updatedProfile.ProfilePawnString;
                    item.SelectSingleNode("ProfileEmail").InnerText = updatedProfile.ProfileEmail;
                    item.SelectSingleNode("ProfilePhone").InnerText = updatedProfile.ProfilePhone;
                    item.SelectSingleNode("PrimaryWeight").InnerText = updatedProfile.PrimaryWeight.ToString();
                    item.SelectSingleNode("CritWeight").InnerText = updatedProfile.CritWeight.ToString();
                    item.SelectSingleNode("VersWeight").InnerText = updatedProfile.VersWeight.ToString();
                    item.SelectSingleNode("HasteWeight").InnerText = updatedProfile.HasteWeight.ToString();
                    item.SelectSingleNode("MastWeight").InnerText = updatedProfile.MastWeight.ToString();
                    item.SelectSingleNode("StamWeight").InnerText = updatedProfile.StamWeight.ToString();
                    item.SelectSingleNode("LeechWeight").InnerText = updatedProfile.LeechWeight.ToString();
                    item.SelectSingleNode("ArmorWeight").InnerText = updatedProfile.ArmorWeight.ToString();
                    item.SelectSingleNode("AvoidanceWeight").InnerText = updatedProfile.AvoidanceWeight.ToString();
                }
            }

            profilesXML.Save("ProfileList.xml");

        }
    }
}
