﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace S00126962_SSD_CA1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //assign on init
        Profile profileRef; 
        XmlDocument classSpecList;
        List<TextBox> editTxtbxs;
        public MainWindow()
        {
            InitializeComponent();
            profileRef = new Profile();
            LoadXML(); //load the Xml in on lanuch
            LoadClassList(); //then load the list of classes from the ClassSpecXML
            GetEditControls();
            PopulateProfilesList(profileRef.GetProfiles());
            
        }

        private void ReadPwnString_Click(object sender, RoutedEventArgs e)
        {
            //send the pawn string off and then get back a profile with those stats
            if (PawnStringTxt.Text == null || PawnStringTxt.Text == "")
                return;

            else
            { 
             Profile readString = profileRef.ReadPawnString(PawnStringTxt.Text);
             PopulateCreateTxts(readString); //populate the fields
            }
        }

        private void PopulateCreateTxts(Profile _pawnReader)
        {
            //run though the feilds and fill in the data
            CharNameTxt.Text = _pawnReader.ProfileName.ToString();
            primWeightTXT.Text = _pawnReader.PrimaryWeight.ToString();
            CritWeightTXT.Text = _pawnReader.CritWeight.ToString();
            HasteWeightTXT.Text = _pawnReader.HasteWeight.ToString();
            VersWeightTXT.Text = _pawnReader.VersWeight.ToString();
            LeechWeightTXT.Text = _pawnReader.LeechWeight.ToString();
            StamWeightTXT.Text = _pawnReader.StamWeight.ToString();
            ArmorWeightTXT.Text = _pawnReader.ArmorWeight.ToString();
            AvoidWeightTXT.Text = _pawnReader.AvoidanceWeight.ToString();
        }

        private void LoadXML()
        {
            //load in the XML
            classSpecList = new XmlDocument();
            classSpecList.Load("ClassSpec.xml");
           
        }
        private void LoadClassList()
        {    //loop though the ClassSpecList xml and grab all the classes
            foreach (XmlNode node in classSpecList.DocumentElement.ChildNodes)
            {
                ClassComBx.Items.Add(node.Name);
                proClassComBx.Items.Add(node.Name);
            }
        }


        private void ClassComBx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SpecComBx.Items.Clear();//clear the box every time this is fired
            string selectedClass = ClassComBx.SelectedItem.ToString();//get the selected class

            //loop though and find the class selected
            foreach (XmlNode node in classSpecList.DocumentElement.ChildNodes)
            {
                if (node.Name == selectedClass)
                {
                    //once it is found,look though the children of that node
                    foreach (var item in node.ChildNodes)
                    {
                        XmlElement spec = (XmlElement)item; //get each as an xml element
                        SpecComBx.Items.Add(spec.InnerText); //ass its inner text to the combobox

                    }
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            //first check if a name is provided
            if (CharNameTxt.Text == null || CharNameTxt.Text == "")
            {
                MessageBox.Show("You need to enter a name");
                return;
            }

            //TODO:Add more checks
            Profile newProfile = new Profile();

            if (CharNameTxt.Text == null || CharNameTxt.Text == "")
            { 
                MessageBox.Show("You need to enter in a name", "Error!");
                return;
            }
            else
                newProfile.ProfileName = CharNameTxt.Text;

            if (EmailTxt.Text == null || EmailTxt.Text == "")
                newProfile.ProfileEmail = "Not Provided";
            else
                newProfile.ProfileEmail = EmailTxt.Text;

            if (PhoneNumTxt.Text == null || PhoneNumTxt.Text == "")
                newProfile.ProfilePhone = "Not Provided";
            else             
            newProfile.ProfilePhone = PhoneNumTxt.Text;
            newProfile.ProfileClass = ClassComBx.SelectedItem.ToString();
            newProfile.ProfileClassSpec = SpecComBx.SelectedItem.ToString();

            if (PawnStringTxt.Text == null || PawnStringTxt.Text == "")
                PawnStringTxt.Text = "Not Provided";
            else
                newProfile.ProfilePawnString = PawnStringTxt.Text;


            if (primWeightTXT.Text == null || primWeightTXT.Text == "")
                newProfile.PrimaryWeight = 0;
            else
            newProfile.PrimaryWeight = Convert.ToDouble(primWeightTXT.Text);

            if (CritWeightTXT.Text == null || CritWeightTXT.Text == "")
                newProfile.CritWeight = 0;
            else
                newProfile.CritWeight = Convert.ToDouble(CritWeightTXT.Text);

            if (HasteWeightTXT.Text == null || HasteWeightTXT.Text == "")
                newProfile.HasteWeight = 0;
            else
                newProfile.HasteWeight = Convert.ToDouble(HasteWeightTXT.Text);

            if (VersWeightTXT.Text == null || VersWeightTXT.Text == "")
                newProfile.VersWeight = 0;
            else
                newProfile.VersWeight = Convert.ToDouble(VersWeightTXT.Text);

            if (StamWeightTXT.Text == null || StamWeightTXT.Text == "")
                newProfile.StamWeight = 0;
            else
                newProfile.VersWeight = Convert.ToDouble(StamWeightTXT.Text);

            if (ArmorWeightTXT.Text == null || ArmorWeightTXT.Text == "")
                newProfile.ArmorWeight = 0;
            else
                newProfile.ArmorWeight = Convert.ToDouble(ArmorWeightTXT.Text);

            if (LeechWeightTXT.Text == null || LeechWeightTXT.Text == "")
                newProfile.LeechWeight = 0;
            else
                newProfile.LeechWeight = Convert.ToDouble(LeechWeightTXT.Text);

            if (AvoidWeightTXT.Text == null || AvoidWeightTXT.Text == "")
                newProfile.AvoidanceWeight = 0;
            else
                newProfile.AvoidanceWeight = Convert.ToDouble(AvoidWeightTXT.Text);

            profileRef.AddProfile(newProfile);
            profilesLst.Items.Clear();
            PopulateProfilesList(profileRef.GetProfiles());
        }

        private void PopulateProfilesList(List<Profile> _profiles)
        {
            foreach (Profile profile in _profiles)
            {
                profilesLst.Items.Add(profile.ProfileName);
            }
        }

        private void profilesLst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //find the profile
            if (profilesLst.SelectedItem == null)
            {
                return;
            }
            Profile selectedProfile = profileRef.GetProfiles().FirstOrDefault(x => x.ProfileName == profilesLst.SelectedItem.ToString());

            //load in the numbers
            proPrimStatTxt.Text = selectedProfile.PrimaryWeight.ToString();
            proCritStatTxt.Text = selectedProfile.CritWeight.ToString();
            proHasteStatTxt.Text = selectedProfile.HasteWeight.ToString();
            proVersStatTxt.Text = selectedProfile.VersWeight.ToString();
            proMsatStatTxt.Text = selectedProfile.MastWeight.ToString();
            proAvoidStatTxt.Text = selectedProfile.AvoidanceWeight.ToString();
            proStamStatTxt.Text = selectedProfile.StamWeight.ToString();
            proLeechStatTxt.Text = selectedProfile.LeechWeight.ToString();
            proArmorTxt.Text = selectedProfile.ArmorWeight.ToString();
            proPhoneTxt.Text = selectedProfile.ProfilePhone.ToString();
            proEmailTxt.Text = selectedProfile.ProfileEmail.ToString();
            proPawnStringTxt.Text = selectedProfile.ProfilePawnString.ToString();
            proCharNameTxt.Text = selectedProfile.ProfileName.ToString();

            //now handle the selection boxes
            proClassComBx.SelectedIndex = proClassComBx.Items.IndexOf(selectedProfile.ProfileClass);

            //load all the nodes first
            foreach (XmlNode node in classSpecList.DocumentElement.ChildNodes)
            {
                if (node.Name == proClassComBx.Text)
                {
                    //once it is found,look though the children of that node
                    foreach (var item in node.ChildNodes)
                    {
                        XmlElement spec = (XmlElement)item; //get each as an xml element
                        proSpecComBx.Items.Add(spec.InnerText); //ass its inner text to the combobox

                    }
                }
            }

            //now select the correct one
            proSpecComBx.SelectedIndex = proSpecComBx.Items.IndexOf(selectedProfile.ProfileClassSpec);
        }

        private void DeleteRecord(object sender, RoutedEventArgs e)
        {
            string selectedProfile = profilesLst.SelectedItem.ToString();
            profilesLst.Items.Clear();
            profileRef.deleteProfile(selectedProfile);
            PopulateProfilesList(profileRef.GetProfiles());
        }

        private void UpdateRecord(object sender, RoutedEventArgs e)
        {

            Profile updatedProfile = new Profile();

            if (proCharNameTxt.Text == null || proCharNameTxt.Text == "")
            {
                MessageBox.Show("You need to enter in a name", "Error!");
                return;
            }
            else
                updatedProfile.ProfileName = proCharNameTxt.Text;


            if (proEmailTxt.Text == null || proEmailTxt.Text == "")
                updatedProfile.ProfileEmail = "Not Provided";
            else
                updatedProfile.ProfileEmail = proEmailTxt.Text;

            if (proPhoneTxt.Text == null || proPhoneTxt.Text == "")
                updatedProfile.ProfilePhone = "Not Provided";
            else
                updatedProfile.ProfilePhone = proPhoneTxt.Text;


            updatedProfile.ProfileClass = proClassComBx.SelectedItem.ToString();
            updatedProfile.ProfileClassSpec = proSpecComBx.SelectedItem.ToString();

            if (proPawnStringTxt.Text == null || proPawnStringTxt.Text == "")
                PawnStringTxt.Text = "Not Provided";
            else
                updatedProfile.ProfilePawnString = proPawnStringTxt.Text;


            if (proPrimStatTxt.Text == null || proPrimStatTxt.Text == "")
                updatedProfile.PrimaryWeight = 0;
            else
                updatedProfile.PrimaryWeight = Convert.ToDouble(proPrimStatTxt.Text);

            if (proCritStatTxt.Text == null || proCritStatTxt.Text == "")
                updatedProfile.CritWeight = 0;
            else
                updatedProfile.CritWeight = Convert.ToDouble(proCritStatTxt.Text);

            if (proHasteStatTxt.Text == null || proHasteStatTxt.Text == "")
                updatedProfile.HasteWeight = 0;
            else
                updatedProfile.HasteWeight = Convert.ToDouble(proHasteStatTxt.Text);

            if (proVersStatTxt.Text == null || proVersStatTxt.Text == "")
                updatedProfile.VersWeight = 0;
            else
                updatedProfile.VersWeight = Convert.ToDouble(proVersStatTxt.Text);

            if (proStamStatTxt.Text == null || proStamStatTxt.Text == "")
                updatedProfile.StamWeight = 0;
            else
                updatedProfile.VersWeight = Convert.ToDouble(proStamStatTxt.Text);

            if (proArmorTxt.Text == null || proArmorTxt.Text == "")
                updatedProfile.ArmorWeight = 0;
            else
                updatedProfile.ArmorWeight = Convert.ToDouble(proArmorTxt.Text);

            if (proLeechStatTxt.Text == null || proLeechStatTxt.Text == "")
                updatedProfile.LeechWeight = 0;
            else
                updatedProfile.LeechWeight = Convert.ToDouble(proLeechStatTxt.Text);

            if (proAvoidStatTxt.Text == null || proAvoidStatTxt.Text == "")
                updatedProfile.AvoidanceWeight = 0;
            else
                updatedProfile.AvoidanceWeight = Convert.ToDouble(proAvoidStatTxt.Text);

            profileRef.UpdateXML(updatedProfile);


            profilesLst.Items.Clear();
            PopulateProfilesList(profileRef.GetProfiles());
            DisableEditing();
        }

        private void EnableEditing(object sender, RoutedEventArgs e)
        {
            if (profilesLst.SelectedIndex == -1)
            {
                return;
            }
            foreach (var item in editTxtbxs)
            {
                item.IsEnabled = true;
            }
            proClassComBx.IsEnabled = true;
            proSpecComBx.IsEnabled = true;
            confirmBtn.IsEnabled = true;
        }

        private void DisableEditing()
        {
            foreach (var item in editTxtbxs)
            {
                item.IsEnabled = false;
            }
        }

        private void GetEditControls()
        {
            editTxtbxs = new List<TextBox>();
            editTxtbxs.Add(proPrimStatTxt);
            editTxtbxs.Add(proCritStatTxt);
            editTxtbxs.Add(proHasteStatTxt);
            editTxtbxs.Add(proVersStatTxt);
            editTxtbxs.Add(proMsatStatTxt);
            editTxtbxs.Add(proAvoidStatTxt);
            editTxtbxs.Add(proStamStatTxt);
            editTxtbxs.Add(proLeechStatTxt);
            editTxtbxs.Add(proArmorTxt);
            editTxtbxs.Add(proPhoneTxt);
            editTxtbxs.Add(proEmailTxt);
        }

        private void proClassComBx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            proSpecComBx.Items.Clear();//clear the box every time this is fired
            string selectedClass = proClassComBx.SelectedItem.ToString();//get the selected class

            //loop though and find the class selected
            foreach (XmlNode node in classSpecList.DocumentElement.ChildNodes)
            {
                if (node.Name == selectedClass)
                {
                    //once it is found,look though the children of that node
                    foreach (var item in node.ChildNodes)
                    {
                        XmlElement spec = (XmlElement)item; //get each as an xml element
                        proSpecComBx.Items.Add(spec.InnerText); //ass its inner text to the combobox

                    }
                }
            }
        }
    }
}
